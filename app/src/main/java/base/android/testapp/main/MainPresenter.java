package base.android.testapp.main;

import base.android.testapp.config.RetrofitConfig;
import base.android.testapp.config.http.CompanyAPI;
import base.android.testapp.models.CompanyAdapter;
import base.android.testapp.models.CompanyData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainPresenter implements MainPresenterContract {

	MainViewContract mainViewContract;
	CompanyAPI companyAPI;

	public MainPresenter(MainViewContract mainViewContract) {
		this.mainViewContract = mainViewContract;
	}

	@Override
	public void getData(final CompanyAdapter companyAdapter) {
		companyAPI = new RetrofitConfig().getRetrofitService();
		Call<CompanyData> companyData = companyAPI.getCompanyData();
		companyData.enqueue(new Callback<CompanyData>() {
			@Override
			public void onResponse(Call<CompanyData> call, Response<CompanyData> response) {
				CompanyData companyData1 = response.body();
				companyAdapter.addItemToList(companyData1);
			}

			@Override
			public void onFailure(Call<CompanyData> call, Throwable t) {
				System.out.println(t.getMessage());
			}
		});
	}


}
