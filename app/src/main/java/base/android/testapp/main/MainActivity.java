package base.android.testapp.main;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;

import base.android.testapp.R;
import base.android.testapp.models.CompanyAdapter;

public class MainActivity extends AppCompatActivity implements MainViewContract {

	MainPresenterContract presenter;
	RecyclerView list;
	RecyclerView.LayoutManager layoutManager;
	CompanyAdapter companyAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		presenter = new MainPresenter(this);

		list = (RecyclerView) findViewById(R.id.list);
		int resId = R.anim.layout_animation_fall_down;
		LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(list.getContext(), resId);
		list.setLayoutAnimation(animation);

		layoutManager = new LinearLayoutManager(this);
		list.setLayoutManager(layoutManager);
		companyAdapter = new CompanyAdapter(this, list);
		list.setAdapter(companyAdapter);
		presenter.getData(companyAdapter);
	}


}
