package base.android.testapp.main;

import base.android.testapp.models.CompanyAdapter;

public interface MainPresenterContract {
	public void getData(CompanyAdapter companyAdapter);
}
