package base.android.testapp.models;

public class CompanyData {

	private Company[] companies;

	private Reach reach;

	private Analysis analysis;

	public Company[] getCompanies ()
	{
		return companies;
	}

	public void setCompanies (Company[] companies)
	{
		this.companies = companies;
	}

	public Reach getReach ()
	{
		return reach;
	}

	public void setReach (Reach reach)
	{
		this.reach = reach;
	}

	public Analysis getAnalysis ()
	{
		return analysis;
	}

	public void setAnalysis (Analysis analysis)
	{
		this.analysis = analysis;
	}

	@Override
	public String toString()
	{
		return "ClassPojo [companies = "+companies+", reach = "+reach+", analysis = "+analysis+"]";
	}
}
