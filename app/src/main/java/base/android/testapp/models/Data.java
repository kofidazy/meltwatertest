package base.android.testapp.models;

public class Data
{
	private String company_id;

	private String change;

	private String value;

	public String getCompany_id ()
	{
		return company_id;
	}

	public void setCompany_id (String company_id)
	{
		this.company_id = company_id;
	}

	public String getChange ()
	{
		return change;
	}

	public void setChange (String change)
	{
		this.change = change;
	}

	public String getValue ()
	{
		return value;
	}

	public void setValue (String value)
	{
		this.value = value;
	}

	@Override
	public String toString()
	{
		return "ClassPojo [company_id = "+company_id+", change = "+change+", value = "+value+"]";
	}
}
