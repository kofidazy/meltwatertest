package base.android.testapp.models;

public class Analysis
{
	private String sub_title;

	private String id;

	private String text;

	private String title;

	private Phrases phrases;

	public String getSub_title ()
	{
		return sub_title;
	}

	public void setSub_title (String sub_title)
	{
		this.sub_title = sub_title;
	}

	public String getId ()
	{
		return id;
	}

	public void setId (String id)
	{
		this.id = id;
	}

	public String getText ()
	{
		return text;
	}

	public void setText (String text)
	{
		this.text = text;
	}

	public String getTitle ()
	{
		return title;
	}

	public void setTitle (String title)
	{
		this.title = title;
	}

	public Phrases getPhrases ()
	{
		return phrases;
	}

	public void setPhrases (Phrases phrases)
	{
		this.phrases = phrases;
	}

	@Override
	public String toString()
	{
		return "ClassPojo [sub_title = "+sub_title+", id = "+id+", text = "+text+", title = "+title+", phrases = "+phrases+"]";
	}
}
