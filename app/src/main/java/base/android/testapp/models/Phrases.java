package base.android.testapp.models;

public class Phrases
{
	private String summary;

	private String performance;

	private String winner;

	private String delta;

	public String getSummary ()
	{
		return summary;
	}

	public void setSummary (String summary)
	{
		this.summary = summary;
	}

	public String getPerformance ()
	{
		return performance;
	}

	public void setPerformance (String performance)
	{
		this.performance = performance;
	}

	public String getWinner ()
	{
		return winner;
	}

	public void setWinner (String winner)
	{
		this.winner = winner;
	}

	public String getDelta ()
	{
		return delta;
	}

	public void setDelta (String delta)
	{
		this.delta = delta;
	}

	@Override
	public String toString()
	{
		return "ClassPojo [summary = "+summary+", performance = "+performance+", winner = "+winner+", delta = "+delta+"]";
	}
}
