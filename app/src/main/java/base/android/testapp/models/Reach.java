package base.android.testapp.models;

public class Reach
{
	private String sub_title;

	private Data[] data;

	private String id;

	private String title;

	public String getSub_title ()
	{
		return sub_title;
	}

	public void setSub_title (String sub_title)
	{
		this.sub_title = sub_title;
	}

	public Data[] getData ()
	{
		return data;
	}

	public void setData (Data[] data)
	{
		this.data = data;
	}

	public String getId ()
	{
		return id;
	}

	public void setId (String id)
	{
		this.id = id;
	}

	public String getTitle ()
	{
		return title;
	}

	public void setTitle (String title)
	{
		this.title = title;
	}

	@Override
	public String toString()
	{
		return "ClassPojo [sub_title = "+sub_title+", data = "+data+", id = "+id+", title = "+title+"]";
	}
}
