package base.android.testapp.models;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.math.MathUtils;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.ValueDependentColor;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import base.android.testapp.R;

import static base.android.testapp.Utils.removeLastChar;
import static base.android.testapp.Utils.round;

public class CompanyAdapter extends RecyclerView.Adapter<CompanyAdapter.ViewHolder> {

	Context context;
	RecyclerView recyclerView;
	List<CompanyData> companyDataList = new ArrayList<>();


	public CompanyAdapter(Context mContext, RecyclerView mRecyclerView) {
		this.context = mContext;
		this.recyclerView = mRecyclerView;
	}

	public void addItemsToList(List<CompanyData> list) {
		companyDataList.clear();
		companyDataList.addAll(list);
		notifyDataSetChanged();
	}

	public void addItemToList(CompanyData l) {
		companyDataList.add(l);
		notifyDataSetChanged();
	}


	@Override
	public CompanyAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
		Context context = viewGroup.getContext();
		LayoutInflater inflater = LayoutInflater.from(context);
		View view = inflater.inflate(R.layout.view_item, viewGroup, false);
		CompanyAdapter.ViewHolder viewHolder = new CompanyAdapter.ViewHolder(view);
		return viewHolder;
	}

	@Override
	public void onBindViewHolder(CompanyAdapter.ViewHolder viewHolder, int position) {
		CompanyData companyData1 = companyDataList.get(position);

		viewHolder.title.setText(companyData1.getAnalysis().getTitle());
		viewHolder.subtitle.setText(companyData1.getAnalysis().getSub_title());
		viewHolder.summary.setText(companyData1.getAnalysis().getPhrases().getSummary());
		viewHolder.winner.setText(companyData1.getAnalysis().getPhrases().getWinner());
		viewHolder.performance.setText(companyData1.getAnalysis().getPhrases().getPerformance());
		viewHolder.delta.setText(companyData1.getAnalysis().getPhrases().getDelta());
		viewHolder.brandreach.setText(companyData1.getReach().getTitle());
		viewHolder.subtitle2.setText(companyData1.getReach().getSub_title());

		StringBuilder stringBuilder = new StringBuilder();

		for (Company s: companyData1.getCompanies()) {
			stringBuilder.append(" " + s.name + " |");
		}

		viewHolder.panels.setText(removeLastChar(stringBuilder.toString()));

		Double a = Double.valueOf(companyData1.getReach().getData()[2].getValue());
		Double b = Double.valueOf(companyData1.getReach().getData()[1].getValue());
		Double c = Double.valueOf(companyData1.getReach().getData()[0].getValue());

		configureCharts(viewHolder.graph, round(a, 2), round(b, 2), round(c, 2));
	}

	@Override
	public int getItemCount() {
		return companyDataList.size();
	}

	public class ViewHolder extends RecyclerView.ViewHolder {

		public TextView title;
		public TextView subtitle;
		public TextView summary;
		public TextView winner;
		public TextView performance;
		public TextView delta;
		public TextView brandreach;
		public TextView subtitle2;
		public TextView panels;
		public GraphView graph;


		public ViewHolder(View itemView) {
			super(itemView);
			title = itemView.findViewById(R.id.title);
			subtitle = itemView.findViewById(R.id.sub_title);
			winner = itemView.findViewById(R.id.winner);
			delta = itemView.findViewById(R.id.delta);
			performance = itemView.findViewById(R.id.performance);
			summary = itemView.findViewById(R.id.summary);
			brandreach = itemView.findViewById(R.id.brandreach);
			subtitle2 = itemView.findViewById(R.id.sub_title2);
			panels = itemView.findViewById(R.id.panels);
			graph = (GraphView) itemView.findViewById(R.id.chart_view);
		}
	}

	public void configureCharts(GraphView graph, double p1, double p2, double p3) {
		BarGraphSeries<DataPoint> series = new BarGraphSeries<>(new DataPoint[]{
				new DataPoint(0, 0),
				new DataPoint(2, p1),
				new DataPoint(5, p2),
				new DataPoint(8, p3),
		});
		graph.addSeries(series);

		series.setValueDependentColor(new ValueDependentColor<DataPoint>() {
			@Override
			public int get(DataPoint data) {
				return Color.rgb(1, 130, 255);
			}
		});

		series.setDrawValuesOnTop(true);
		series.isAnimated();
		series.setValuesOnTopColor(Color.BLACK);
	}

}

