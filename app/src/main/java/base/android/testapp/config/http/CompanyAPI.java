package base.android.testapp.config.http;

import java.util.List;

import base.android.testapp.models.CompanyData;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface CompanyAPI {

	@GET("bins/1eenaa")
	Call<CompanyData> getCompanyData();
}
