package base.android.testapp.config;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import base.android.testapp.Constants;
import base.android.testapp.config.http.CompanyAPI;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitConfig {

	public CompanyAPI getRetrofitService() {

		Gson gson = new GsonBuilder()
				.setLenient()
				.create();

		Retrofit retrofit = new Retrofit.Builder()
				.baseUrl(Constants.baseUrl)
				.addConverterFactory(GsonConverterFactory.create(gson))
				.build();

		CompanyAPI service = retrofit.create(CompanyAPI.class);

		return service;
	}



}
