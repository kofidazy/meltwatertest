package base.android.testapp;

import org.junit.Before;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import base.android.testapp.main.MainPresenter;
import base.android.testapp.main.MainViewContract;

public class MainPresenterTest {

	private MainPresenter mainPresenter;

	@Mock
	private MainViewContract mainViewContract;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		mainPresenter = Mockito.spy(new MainPresenter(mainViewContract));
	}

//	@Test
//	public void sumTestCorrect() {
//		int a = 1;
//		int b = 2;
//		//Trigger
//		int res = mainPresenter.sum(1, 2);
//		//Validation
//		assertEquals(res, 3);
//	}


}
