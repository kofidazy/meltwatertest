package base.android.testapp;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import base.android.testapp.main.MainPresenter;
import base.android.testapp.main.MainViewContract;

import static org.junit.Assert.assertEquals;

public class UtilsTest {

	private Utils utils;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		utils = Mockito.spy(new Utils());
	}

	@Test
	public void canRemoveLastCharacter() {
		assertEquals("do", Utils.removeLastChar("dog"));
	}

	@Test
	public void canRoundToDecimalPlace() {
		assertEquals(9.12, Utils.round(9.118,2),0.00);
	}


}
